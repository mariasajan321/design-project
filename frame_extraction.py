# Program To Read video
# and Extract Frames
import cv2


def FrameCapture(path):
    vidObj = cv2.VideoCapture(path)

    count = 0

    success = 1

    while success:
        # vidObj object calls read
        # function extract frames
        success, image = vidObj.read()

        # Saves the frames with frame-count
        #cv2.imwrite("frame%d.jpg" % count, image)

        count += 1
    print("Total number of frames:",count)

# Driver Code
if __name__ == '__main__':
    # Calling the function
    FrameCapture("/home/maria/PycharmProjects/design project/tutorial3.mp4")
