import cv2
import numpy as np
import time
from numpy import vstack

start_time = time.time()
video_path = "/home/maria/PycharmProjects/design project/tutorial3.mp4"
#p_frame_thresh = 2000000/4  # You may need to adjust this threshold
cap = cv2.VideoCapture(video_path)
# Read the first frame.
ret, prev_frame = cap.read()
h, w = prev_frame.shape[0], prev_frame.shape[1]
p_frame_thresh = 125000
print(p_frame_thresh)
print(h, w)
count = 0
temp_frame=[]
i=0
#prev_frame = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)
f=0

def fun(seg1,seg2):
    diff = np.absolute(np.array(seg1) - np.array(seg2))
    #print(diff)
    non_zero_count = np.count_nonzero(diff)
    return(non_zero_count)

def segment( curr_frame , prev_frame ,h ,w):
    a1 = curr_frame[:int(h / 4 - 1), :int(w - 1)]
    a2 = curr_frame[int(h / 4):int(h / 2 - 1), :int(w - 1)]
    a3 = curr_frame[int(h / 2):int((3 * h )/ 4 - 1), :int(w - 1)]
    a4 = curr_frame[int((3 * h) / 4):int(h - 1), :int(w - 1)]
    a5 = np.vstack(([a1], [a2], [a3], [a4]))  # current frame segments in a stack

    a11 = prev_frame[:int(h / 4 - 1), :int(w - 1)]
    a22 = prev_frame[int(h / 4):int(h / 2 - 1), :int(w - 1)]
    a33 = prev_frame[int(h / 2):int((3 * h) / 4 - 1), :int(w - 1)]
    a44 = prev_frame[int((3 * h) / 4):int(h - 1), :int(w - 1)]
    a55 = np.vstack(([a11], [a22], [a33], [a44]))  # previous frame segments in a stack
    #print(a5)
    #print(a55)
    return a5, a55


while ret:
    f+=1
    #print(f)
    prev=i
    #print("prev=",prev)
    ret, curr_frame = cap.read()
    if(ret==False):
        break
    #curr_frame = cv2.cvtColor(curr_frame, cv2.COLOR_BGR2GRAY)
    a5, a55 = segment(curr_frame, prev_frame, h, w)

    flag = 0
    if ret:
        #for i in (0, 1, 2, 3):
            #temp = (i + 1) * int(h / 4 - 1)
        i=0
        s1 = a5[i]
        s2 = a55[i]
        non_zero_count=fun(s1,s2)
        #print("non_zero",non_zero_count)
        if non_zero_count > p_frame_thresh:
            flag = 1
        else:
            flag = 0

        if flag==1:
            #if prev==4:
                cv2.imwrite("frame%d.jpg" % count, prev_frame)
                count += 1
                print("count=",count)
        prev_frame=curr_frame

        #if flag == 1 & i == 4:
            #print(i)
            #cv2.imwrite("frame%d.jpg" % count, temp_frame)
            #count += 1
        #else:
            #curr_frame = cv2.cvtColor(curr_frame, cv2.COLOR_GRAY2BGR)
         #   temp_frame = curr_frame

        #prev_frame = curr_frame
print("no. of frames:",count)
print("--- %s seconds ---" % (time.time() - start_time))
