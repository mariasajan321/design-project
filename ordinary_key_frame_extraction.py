import time
start_time = time.time()
import cv2
import numpy as np

video_path = "/home/maria/test.mp4"
p_frame_thresh = 600000 # You may need to adjust this threshold

cap = cv2.VideoCapture(video_path)
# Read the first frame.
ret, prev_frame = cap.read()
count=0



while ret:
    ret, curr_frame = cap.read()

    if ret:
        diff = cv2.absdiff(curr_frame, prev_frame)
        non_zero_count = np.count_nonzero(diff)
        if non_zero_count > p_frame_thresh:
            cv2.imwrite("keyframe%d.jpg" % count, curr_frame)
            count+=1
        prev_frame = curr_frame
print("Total number of keyframes=",count)
print("--- %s seconds ---" % (time.time() - start_time))
