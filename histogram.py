import cv2
import numpy as np
import matplotlib.pyplot as plt
import numpy as np
import time


start_time = time.time()
video_path = "/home/linda/test2.mp4"

cap = cv2.VideoCapture(video_path)
ret, prev_frame = cap.read()
count=0
while ret:
    ret, curr_frame = cap.read()

    if ret:


        imgGray = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)
        ghist = cv2.calcHist([imgGray], [0], None, [256], [0,256])
        imgGray1 = cv2.cvtColor(curr_frame, cv2.COLOR_BGR2GRAY)
        ghist1 = cv2.calcHist([imgGray1], [0], None, [256], [0, 256])

        diff = cv2.absdiff(ghist,ghist1)
        non_zero_count = np.count_nonzero(diff)

        a=np.mean(diff).item()
        b=np.std(diff).item()
        #print("Mean = {:.1f}, standard deviation = {:.1f}".format(a,b))

        p_frame_thresh = a + b;
        if non_zero_count > p_frame_thresh:
            cv2.imwrite("keyframe%d.jpg" % count, curr_frame)
            count += 1
        prev_frame = curr_frame
print("Total number of frames:", count)
